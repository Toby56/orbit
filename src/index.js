const { app, BrowserWindow, BrowserView, session, Menu, Tray, shell } = require('electron').remote;

const remote = require('electron').remote;

var fs = require('fs');

let win = remote.getCurrentWindow();

win.once('ready-to-show', () => {
  win.show();
});

let view = new BrowserView();
win.setBrowserView(view);

view.setAutoResize({
  width: true,
  height: true
});

view.webContents.loadURL('https://boxhillhs-vic.compass.education');

view.webContents.on('dom-ready', () => {
  view.webContents.insertCSS(fs.readFileSync('src/res/compass-styles.css', 'utf8'), {
    cssOrigin: 'user'
  });
})

view.webContents.on('new-window', (event, url) => {
  event.preventDefault()
  shell.openExternal(url)
})

viewSizing();
function viewSizing() {
  windowBounds = win.getBounds();
  console.log(windowBounds);
  if (win.isMaximized()) {
    view.setBounds({
      x: 0,
      y: 32,
      width: windowBounds.width - 16,
      height: windowBounds.height - 32 - 16
    });
  } else {
    if (win.isFullScreen()) {
      view.setBounds({
        x: 0,
        y: 32,
        width: windowBounds.width,
        height: windowBounds.height - 32
      });
    } else {
      view.setBounds({
        x: 1,
        y: 33,
        width: windowBounds.width - 2,
        height: windowBounds.height - 34
      });
    }
  }
}

document.onreadystatechange = () => {
  if (document.readyState == "complete") {
    document.getElementById('min-button').addEventListener("click", event => {
      win.minimize();
    });

    document.getElementById('max-button').addEventListener("click", event => {
      win.maximize();
    });

    document.getElementById('restore-button').addEventListener("click", event => {
      if (win.isFullScreen()) {
        win.setFullScreen(false);
      } else {
        win.unmaximize();
      }
    });

    document.getElementById('close-button').addEventListener("click", event => {
      win.close();
    });

    toggleMaximized();
    win.on('maximize', toggleMaximized);
    win.on('unmaximize', toggleMaximized);

    function toggleMaximized() {
      viewSizing();
      if (win.isMaximized()) {
        document.body.classList.add('maximized');
      } else {
        document.body.classList.remove('maximized');
      }
    }

    toggleBlurred();
    win.on('blur', toggleBlurred);
    win.on('focus', toggleBlurred);

    function toggleBlurred() {
      if (win.isFocused()) {
        document.body.classList.remove('blurred');
      } else {
        document.body.classList.add('blurred');
      }
    }

    toggleFullScreen();
    win.on('enter-full-screen', toggleFullScreen);
    win.on('leave-full-screen', toggleFullScreen);

    function toggleFullScreen() {
      viewSizing()
      if (win.isFullScreen()) {
        document.body.classList.add('full-screen');
      } else {
        document.body.classList.remove('full-screen');
      }
    }
  }
};
