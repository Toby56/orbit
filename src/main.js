const { app, BrowserWindow } = require('electron')

const windowStateKeeper = require('electron-window-state');

app.on('ready', () => {
  let oldWindowState = windowStateKeeper({
    defaultWidth: 1000,
    defaultHeight: 600
  });

  win = new BrowserWindow({
    x: oldWindowState.x,
    y: oldWindowState.y,
    width: oldWindowState.width,
    height: oldWindowState.height,
    frame: false,
    backgroundColor: '#FFF',
    webPreferences: {
        nodeIntegration: true
    },
    show: false
  });

  oldWindowState.manage(win);

  win.loadFile('src/index.html');
})
